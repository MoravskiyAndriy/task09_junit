package com.moravskiyandriy.arrayanalyzer;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ArrayAnalyzerTest {
    private static final int TESTS_NUMBER = 4;
    private static int counter = 0;
    private static List<Integer[][]> data;

    @BeforeAll
    private static void initialize() {
        data = new ArrayList<>();
        data.add(new Integer[][]{{1, 2, 2, 2, 1, 3, 4, 5, 6}, {1, 3}});
        data.add(new Integer[][]{{1, 4, 2, 2, 1, 3, 4, 7, 7, 7, 5, 5, 6}, {7, 3}});
        data.add(new Integer[][]{{1, 4, 4}, {0, 0}});
        data.add(new Integer[][]{{}, {0, 0}});
    }

    @RepeatedTest(TESTS_NUMBER)
    @DisplayName("analyzeMethod")
    void analyzeMethodTest() {
        Integer[] methodResult = new ArrayAnalyzer(data.get(counter)[0]).analyze();
        Integer[] expected = data.get(counter)[1];
        counter++;
        assertArrayEquals(methodResult, expected);
    }
}

