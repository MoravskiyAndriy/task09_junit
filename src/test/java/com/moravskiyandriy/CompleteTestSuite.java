package com.moravskiyandriy;

import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.SelectPackages;
import org.junit.runner.RunWith;

@RunWith(JUnitPlatform.class)
@SelectPackages({"com.moravskiyandriy.arrayanalyzer","com.moravskiyandriy.miner"})
public class CompleteTestSuite
{
}
