package com.moravskiyandriy.miner;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.Mockito;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

class MinerTest {
    private Miner miner = Mockito.mock(Miner.class, Mockito.CALLS_REAL_METHODS);

    @ParameterizedTest(name = "{index} => a={0}, b={1}, probability={2}")
    @CsvSource({
            "1, 1, 2.0",
            "2, 3, 0.5",
            "-3, -2, 100",
            "5, 5, 0.0"
    })
    void initTest(int a, int b, double probability) {
        assertTrue(miner.initialize(a,b,probability));
    }

    @ParameterizedTest(name = "{index} => a={0}, b={1}, probability={2}")
    @CsvSource({
            "1, 1, 2.0",
            "2, 3, 0.5",
            "-3, -2, 100",
            "5, 5, 0.0"
    })
    void fieldFormingTest(int a, int b, double probability) {
        miner.initialize(a,b,probability);
        assertTrue(miner.formField());
    }

    @ParameterizedTest(name = "{index} => a={0}, b={1}, probability={2}")
    @CsvSource({
            "1, 1, 2.0",
            "2, 3, 0.5",
            "-3, -2, 100",
            "5, 5, 0.0"
    })
    void minedFieldFormingIncorrectTest(int a, int b, double probability) {
        miner.initialize(a,b,probability);
        assertFalse(miner.formMinedField());
    }

    @ParameterizedTest(name = "{index} => a={0}, b={1}, probability={2}")
    @CsvSource({
            "1, 1, 2.0",
            "2, 3, 0.5",
            "-3, -2, 100",
            "5, 5, 0.0"
    })
    void minedFieldFormingCorrectTest(int a, int b, double probability) {
        miner.initialize(a,b,probability);
        miner.formField();
        assertTrue(miner.formMinedField());
    }

    @ParameterizedTest(name = "{index} => a={0}, b={1}, probability={2}")
    @CsvSource({
            "1, 1, 2.0",
            "2, 3, 0.5",
            "-3, -2, 100",
            "5, 5, 0.0"
    })
    void showFieldsIncorrectTest1(int a, int b, double probability) {
        miner.initialize(a,b,probability);
        assertFalse(miner.showFields());
    }

    @ParameterizedTest(name = "{index} => a={0}, b={1}, probability={2}")
    @CsvSource({
            "1, 1, 2.0",
            "2, 3, 0.5",
            "-3, -2, 100",
            "5, 5, 0.0"
    })
    void showFieldsIncorrectTest2(int a, int b, double probability) {
        miner.initialize(a,b,probability);
        miner.formField();
        assertFalse(miner.showFields());
    }

    @ParameterizedTest(name = "{index} => a={0}, b={1}, probability={2}")
    @CsvSource({
            "1, 1, 2.0",
            "2, 3, 0.5",
            "-3, -2, 100",
            "5, 5, 0.0"
    })
    void showFieldsCorrectTest(int a, int b, double probability) {
        miner.initialize(a,b,probability);
        miner.formField();
        miner.formMinedField();
        assertTrue(miner.showFields());
    }

}
