package com.moravskiyandriy.arrayanalyzer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Objects;

public class ArrayAnalyzer {
    private static final Logger logger = LogManager.getLogger(ArrayAnalyzer.class);
    private Integer[] array;

    public ArrayAnalyzer(Integer[] array) {
        this.array = array;
    }

    public Integer[] analyze() {
        if (Objects.isNull(array) || array.length < 3) {
            logger.info("Array is not suitable for the operation.");
            return new Integer[]{0, 0};
        }
        int finalPosition = 0;
        int finalLength = 0;
        for (int i = 0; i < array.length - 1; i++) {
            if (array[i] < array[i + 1]) {
                int position = i + 1;
                int counter = i + 1;
                int len = 1;
                while (counter < array.length - 1 && array[counter].equals(array[counter + 1])) {
                    counter++;
                    len++;
                }
                if (counter < array.length - 1 && array[counter + 1] < array[counter]) {
                    if (len > finalLength) {
                        finalLength = len;
                        finalPosition = position;
                    }
                }
            }
        }
        return new Integer[]{finalPosition, finalLength};
    }
}
