package com.moravskiyandriy.miner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Random;
import java.util.stream.IntStream;

public abstract class Miner {
    private static final Logger logger = LogManager.getLogger(Miner.class);
    private int height;
    private int width;
    private double probability;
    private char[][] field;
    private int[][] minedField;
    private boolean initVerification;
    private boolean fieldFormedVerification;
    private boolean mineFieldFormedVerification;

    public boolean initialize(int height, int width, double probability) {
        try {
            this.height = Math.abs(height) + 2;
            this.width = Math.abs(width) + 2;
            this.probability = Math.abs(probability) > 1 ? 1 : Math.abs(probability);
            field = new char[this.height][this.width];
            minedField = new int[this.height][this.width];
            initVerification = true;
            return true;
        } catch (Exception e){
            logger.info("Initialization failed, see details:");
            e.printStackTrace();
            return false;
        }
    }

    public boolean formField() {
        if(initVerification) {
            IntStream.range(0, height * width).forEach(n -> {
                int i = n / width;
                int j = n % width;
                field[i][j] = new Random().nextDouble() <= probability ? '*' : '.';
            });
            fieldFormedVerification = true;
            return true;
        }
        return false;
    }

    private boolean check(int a, int b) {
        return a >= 0 && b >= 0 && a < height && b < width;
    }

    public boolean formMinedField() {
        if(fieldFormedVerification) {
            for (int i = 0; i < height; i++) {
                for (int j = 0; j < width; j++) {
                    if (field[i][j] == '.') {
                        int counter = 0;
                        for (int k = i - 1; k <= i + 1; k++) {
                            for (int t = j - 1; t <= j + 1; t++) {
                                if (check(k, t)) {
                                    if (field[k][t] == '*') {
                                        counter++;
                                    }
                                }
                            }
                        }
                        minedField[i][j] = counter;
                    }
                }
            }
            mineFieldFormedVerification = true;
            return true;
        }
        else{
            return false;
        }
    }

    public boolean showFields() {
        if(mineFieldFormedVerification) {
            logger.info("\nMinedField:");
            for (char[] arr : field) {
                StringBuilder str = new StringBuilder();
                for (char val : arr) {
                    str.append(val).append(" ");
                }
                logger.info(str.toString());
            }
            logger.info("\nNumbersField:");
            for (int[] arr : minedField) {
                StringBuilder str = new StringBuilder();
                for (int val : arr) {
                    str.append(val).append(" ");
                }
                logger.info(str.toString());
            }
            return true;
        }
        return false;
    }

    public abstract boolean mokitoReason();
}
