package com.moravskiyandriy;

import com.moravskiyandriy.arrayanalyzer.ArrayAnalyzer;
import com.moravskiyandriy.miner.Miner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;

public class Application {
    private static final Logger logger = LogManager.getLogger(Application.class);

    private static void minerTest() {
        Miner miner = new Miner(){
            @Override
            public boolean mokitoReason() {
                return false;
            }
        };
        logger.info("Miner:");
        miner.initialize(3, 5, 0.5);
        miner.formField();
        miner.formMinedField();
        miner.showFields();
    }

    private static void arrayAnalyzerTest() {
        StringBuilder arrayDisplay= new StringBuilder();
        Integer[] array = {1, 2, 3, 3, 3, 1, 7, 1, 4, 4, 4, 1, 8, 8, 7};
        ArrayAnalyzer arrayAnalyzer = new ArrayAnalyzer(array);
        Integer[] result = arrayAnalyzer.analyze();
        for(Integer val: array){
            arrayDisplay.append(val.toString()).append(" ");
        }
        logger.info(arrayDisplay.toString()+"\nResult: position= "+result[0]+", length= "+result[1]+".\n");
    }

    public static void main(String[] args) {
        arrayAnalyzerTest();
        minerTest();
    }
}
